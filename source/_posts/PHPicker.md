---
title: IOS14-PHPicker 选择照片和视频
---
前言
    针对IOS的应用大多数都会和相册打交道，选择照片或者视频，用于设置头像，发个动态等等，但是往往用户只需要选择一张图片时，APP通过授权就能获取到用户整个相册所有照片，这对用户隐私有着严重泄漏风险，苹果一直注重用户隐私保护，所有在IOS14之后苹果在相册做了很大改变。

相册改变
    1.相册权限变动：IOS14后苹果引入了一个新的权限状态：limited 当我们通过 requestAuthorization 向用户获取权限时，弹窗中多了一个选项：“选择部分照片”，此时系统会在 App 进程之外弹出相册让用户选择授权给当前 App 访问的照片。用户完成选择后，App 通过相册相关的 api 就只能获取到指定的这几张照片，这有效的保护了用户的隐私，这个设计虽然保护了用户的隐私，但是从 App 的角度来看却引入了新的问题：如果我们还是用原先的那一套流程来获取照片，那么在用户选择了部分权限的情况下，每次弹出相册后用户都无法选择其他的照片，解决办法是再次弹出权限询问，让用户选择或者更换指定的照片，然后再回到我们的相册 UI，这一套流程明显更加复杂，降低了用户体验。
    2.新的相册组件：PHPicker 是 iOS 14 引入的一套全新的相册 API，通过它访问相册，可以不需要向用户获取相关权限，而且具有与系统相册一致的 UI 和操作方式，可以保证用户体验的一致性。并且和相册 App 一样，支持通过人物、地点等关键信息来搜索照片。并且 PHPicker 是在独立进程中运行的，与宿主 App 无关，宿主 App 也无法通过截屏 api 来获取当前屏幕上的照片信息（这篇文章主要就是说这个）。

PHPicker 使用

    1.声明 PHPickerConfiguration，进行配置，再传给 PHPickerViewController，完成调用环节，代码如下：
var config = PHPickerConfiguration()
// 可选择的资源数量，0表示不设限制，默认为1
config.selectionLimit = 0
// 可选择的资源类型
// 只显示图片（注：images 包含 livePhotos）
config.filter = .images
// 显示 Live Photos 和视频（注：livePhotos 不包含 images）
config.filter = .any(of: [.livePhotos, .videos])
// 如果要获取视频，最好设置该属性，避免系统对视频进行转码
config.preferredAssetRepresentationMode = .current

let picker = PHPickerViewController(configuration: config)
picker.delegate = self
present(picker, animated: true, completion: nil)

    2.处理 PHPicker 的回调 PHPicker 的代理方法只有一个（注意： 取消选择也会触发代理方法，会返回空的 results），声明如下：
@available(iOS 14, *)
public protocol PHPickerViewControllerDelegate : AnyObject {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult])
}
     
    3.如何获取照片？ 获取照片比较简单，通过 NSItemProvider 的 loadObject 方法，并且指定 Class 类型为 UIImage，就可以在回调中得到 UIImage 类型的照片了（获取 LivePhoto 与获取照片类似，只是需要将 UIImage 替换为 PHLivePhoto。之后你可以通过 PHLivePhotoView 来显示。或者通过 PHAssetResourceManager 获取 LivePhoto 的原始数据）代码如下：
func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
    // 首先需要 dismiss picker
    picker.dismiss(animated: true, completion: nil)
    for result in results {
        // 判断类型是否为 UIImage
        if result.itemProvider.canLoadObject(ofClass: UIImage.self) {
            // 确认类型后，调用 loadObject 方法获取图片
            result.itemProvider.loadObject(ofClass: UIImage.self) { (data, error) in
                // 回调结果是在异步线程，展示时需要切换到主线程
                if let image = data as? UIImage {
                    DispatchQueue.main.async {
                        self.showImage(image)
                    }
                }
            }
        }
    }
}
     
    4.如何获取视频？获取视频稍微复杂一点，框架开发者在官方论坛中明确指出需要使用 loadFileRepresentation 方法来加载大文件，loadFileRepresentation 的使用方式与 UIImage 类似，但需要额外传入一个参数 forTypeIdentifier 来指定文件类型，指定为 public.movie 可以覆盖相册中的 .mov 和 .mp4 类型。与照片不同的是，这个 api 返回的是一个 URL 类型的临时文件路径，苹果在这个 API 的说明中指出：系统会把请求的文件数据复制到这个路径对应的地址，并且在回调执行完毕后删除临时文件。代码如下：
func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
    // 首先需要 dismiss picker
    picker.dismiss(animated: true, completion: nil)
    for result in results {
        if result.itemProvider.canLoadObject(ofClass: UIImage.self) {
            // 判断类型是否为 UIImage
            ...
        } else {
            // 类型为 Video
            // 调用 loadFileRepresentation 方法获取视频的 url
            // 这里 Type Identifier 我们用 UTType.movie.identifier (“public.movie”) 这个 UTI 可以获取所有格式的视频
            result.itemProvider.loadFileRepresentation(forTypeIdentifier: UTType.movie.identifier) { (url, error) in
                if let error = error {
                    print(error)
                    return
                }
                    // 系统会将视频文件存放到 tmp 文件夹下
                    // 我们必须在这个回调结束前，将视频拷贝出去，一旦回调结束，系统就会把视频删掉
                    // 所以一定要确定拷贝结束后，再切换到主线程做 UI 操作
                    // 另外不用担心视频过大而导致拷贝的时间很久，系统将创建一个 APFS 的克隆项，因此拷贝的速度会非常快
                    guard let url = url else { return }
                    let fileName = "\(Int(Date().timeIntervalSince1970)).\(url.pathExtension)"
                    let newUrl = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
                    try? FileManager.default.copyItem(at: url, to: newUrl)
                    DispatchQueue.main.async {
                        self.playVideo(newUrl)
                    }
            }
        }
    }
}

iCloud同步
*  iOS 相册提供了 iCloud 同步功能，如果用户开启了相册同步，那么相册中的照片、视频或者 LivePhoto 有可能会被上传到 iCloud，而本地只保存有缩略图，当请求某张照片时，相册会先从 iCloud 下载，然后再返回数据。
*  在以前具有完整访问权限时，App 可以获得资源是否存在 iCloud 的状态，并且在下载时获得进度信息。由于 PHPicker 向 App 隐藏了所有隐私信息，因此我们无法再得知资源的 iCloud 同步状态，PHPicker 会自动从 iCloud 下载资源，并且完成之后通过 delegate 回调将数据返回，但是宿主 App 是无法从 PHPicker 中获取到 iCloud 的下载进度信息的，所以暴露出一个问题就是如果用户选择了比较大的视频，或者是网络状态不好的话，视频导出依然需要耗费非常长的时间，并且没有进度信息！

总结 
PHPicker 的集成本身很简单，也存在一定的优缺点:
优点：
* 保护用户隐私不需要频繁询问相册权限
* 对于用户体验提升较大
* 行为逻辑与系统相册保持一致，降低了用户的学习成本
* 集成简单，在最低支持版本 iOS 14
缺点：
* 太过简单，选择的图片没有进行编辑，选择视频也没有时长限制，
* iCloud 大文件下载缺少进度信息相当于加载一个文件不知道什么时候能结束，没有用户体验。

